# How to run in production #

In production, how to run your container depends on what
infrastructure you have. 

For the network part, you need a reverse proxy and redirect the http
requests to the port 8050 inside the container.

For the static configurations, all of them are done with environment
variables. The only configuration who are not optionnal is:
  * `WIKI_SECRET_KEY`: The secret key used for password hash
  
But I also need to set these configurations:
  * `WIKI_ALLOWED_HOSTS`: The host names of your wiki
  * `WIKI_EMAIL_*`: The e-mail configurations, needed to send account
activation e-mails

See the document file [Configuration](configuration.md) for more
information.

For the database, you have to choose between sqlite and postgresql. By
default, it use sqlite. See the sub-section `Database settings` of the
document file [Configuration](configuration.md) for more
information.

See the document file [Configuration](volumes.md) for more information
about the sqlite storage.

For the static and media files, you have to distribute them with an
http server. They are stored in the folders `/static` and `/media`
inside the container. 

See the document file [Configuration](volumes.md) for more information.

To collect static files in `/static`, you have to run this command inside 
your container:
```sh
python manage.py collectstatic
```

For the reverse proxy, I suggest Caddy. It generate automaticaly a Let's
Encrypt certificat for your domain and can distribute the static files.
