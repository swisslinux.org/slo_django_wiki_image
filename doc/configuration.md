# Configuration ##

When you run a container from this image, you can configure it with
environment variables.


## Main settings ###

  * `WIKI_SECRET_KEY`: Django secret key, for default see
    `slo_django_wiki/settings.py`
  * `WIKI_DEBUG`: Activate the debug, for dev, default is `False`
  * `WIKI_ALLOWED_HOSTS`: List of allowed host names, like the domaine
    of your wiki, default is `127.0.0.1, localhost`
  * `WIKI_REVERSE_PROXY`: If the wiki run behind a reverse proxy, default `no`


## Database settings ###

  * `WIKI_DATABASE_ENGINE`: The database engine to use, possibles
    `sqlite` and `postgresql`, default at `sqlite`
  * `WIKI_DATABASE_PG_BASE_NAME`: Postgresql database name, default is
    `slo_wiki`
  * `WIKI_DATABASE_PG_USER`: Postgresql database user, default is
    `slo_wiki`
  * `WIKI_DATABASE_PG_PASSWORD`: Postgresql database password, default
    is empty
  * `WIKI_DATABASE_PG_HOST`: Postgresql database host, default is
    `127.0.0.1`
  * `WIKI_DATABASE_PG_PORT`: Postgresql database port, default is
    `5432`

Note: With `sqlite` database engine, the database is stored in `/sqlite/db.sqlite3`


## Internationalization ###

  * `WIKI_LANGUAGE_CODE`: Language code, default is `fr-ch`
  * `WIKI_TIME_ZONE`: Time zone, default is `Europe/Zurich`


## Static files ###

  * `WIKI_STATIC_URL`: Static files URL, default `/static/`
  * `WIKI_STATIC_ROOT`: Static files directories, default is `/static`
  * `WIKI_MEDIA_URL`: Media files URL, default `/media/`
  * `WIKI_MEDIA_ROOT`: Root directory for storing media files, default is `/media`
  
  
## Registration ###

  * `WIKI_ACCOUNT_ACTIVATION_DAYS`: Number of days users will have to
    activate their accounts after registering, default is 7


## Email ###

  * `WIKI_EMAIL_BACKEND`: The backend to use, choice is `console` or
    `smtp`, default `console` [1]
  * `WIKI_DEFAULT_FROM_EMAIL`: Default email address to use in the
    "from" field
  * `WIKI_EMAIL_HOST`: The host to use for sending email, default "localhost"
  * `WIKI_EMAIL_PORT`: Port to use for the SMTP server defined in
    `WIKI_EMAIL_HOST`, default 25
  * `WIKI_EMAIL_USE_TLS`: Whether to use a TLS (secure) connection
    when talking to the SMTP server, default "False"
  * `WIKI_EMAIL_USE_SSL`: Whether to use a SSL (secure) connection
    when talking to the SMTP server, default "False"
  * `WIKI_EMAIL_HOST_USER`: Username to use for the SMTP server
    defined in `WIKI_EMAIL_HOST`, default empty [2]
  * `WIKI_EMAIL_HOST_PASSWORD`: Password to use for the SMTP server
    defined in `WIKI_EMAIL_HOST`, default is empty



[1] If you choose `console` backend, the email are not sent but
printed in the STDOUT. It's useful for development.

[2] If empty, it won’t attempt authentication.
