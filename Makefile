container_engine = podman


build-dev:
	$(container_engine) build -t slo_wiki_dev:latest -f Containerfile.dev

run-dev:
	$(container_engine) run -it --rm -p 8000:8000 -v .:/code:z --name slo_wiki_dev localhost/slo_wiki_dev

persistant-volume:
	$(container_engine) volume create slo_wiki_db
	$(container_engine) volume create slo_wiki_static
	$(container_engine) volume create slo_wiki_media

run-dev-persistant:
	$(container_engine) run -it --rm -p 8000:8000 -v .:/code:z -v slo_wiki_db:/sqlite -v slo_wiki_static:/static -v slo_wiki_media:/media --name slo_wiki_dev localhost/slo_wiki_dev

build-prod:
	$(container_engine) build -t slo_wiki:latest -f Containerfile.prod
