# Container Image of Django Wiki #

By Swisslinux.org.


This is a container image we use at Swisslinux.org to provide us a
Wiki.


## What it's made from ##

This Wiki is a django project made with 2 components:
  * Django Wiki
  * Django Registration

More informations about Django Wiki:
[Source code](https://github.com/django-wiki/django-wiki)
[Documentation](https://django-wiki.readthedocs.io/en/latest/)

More informations about Django Registration:
[Source code](https://github.com/ubernostrum/django-registration)
[Documentation](https://django-registration.readthedocs.io/)


## How to build the image ##

For developpment:

```sh
make build-dev
```


For production:

```sh
make build-prod
```


## How to run the container ##

### For developpment ###

First, start the container:
```sh
make run-dev
```

Then, exec bash inside this container:
```sh
podman exec -it slo_wiki_dev bash
```

Now, you can run the migration scripts for the database:
```sh
python manage.py migrate
```

And create a super user:
```sh
python manage.py createsuperuser
```

Note: The database use sqlite and is stored inside the container. So,
if you remove the container, you lose the database. To avoid it, you
can run these 2 commands instead of the first one:
```sh
make persistant-volume
make run-dev-persistant
```


### For production ###

See the document file [Run in production](doc/run-in-producton.md).


## Important Notes ##

### About translation ###

If you generate translation files from the templates, Django will
generate the translations for the template `wiki/base_site.html`. You
can ignore them, as this template is a copy from Django-wiki with a
few modification that already have translation.

Of course, if you add more modifications to this template, and you
need to translate them, you don't have to ignore the translation
entries for your modifications.


### About the templates ###

This website provide some additionnal templates. For account creation,
connexion, etc.

It also provide a copy of the template `wiki/base_site.html` from
Django-wiki with one modification: The add of en entry in the user
menu, a link to the page to modify the user password.


## Licence ##

This website and Django-wiki use the GPLv3 licence.


## Author ##

Sébastien Gendre <seb@k-7.ch>
