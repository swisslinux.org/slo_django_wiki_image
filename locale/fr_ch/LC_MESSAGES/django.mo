��    )      d  ;   �      �  <   �  �   �  0   f  k   �  @     -   D  o   r  �   �  -   �     �     �     �  4   �     +     <     Y  5   a     �  i   �               '     -     9     O     `     x     �  "   �     �     �     �     	     4	     C	     \	  @   m	  B   �	  ,   �	     
    9
  O   Q  �   �  5   D  k   z  F   �  4   -  �   b  �   �  =   �     �     �        4   #     X      l  	   �  J   �     �  �   �     �     �  	   �     �     �     �  )     3   ,  .   `  A   �  /   �            .   .     ]  1   n     �  ^   �  E     I   c     �     #                 !                           
      %            "                $                    )                     '   &                                   (                           	          

Best regards
––––––––
Swisslinux.org team
 

You have %(expiration_days)s days before the expiration of this activation link.

Best regards.
––––––––
Swisslinux.org team
 
        Account activation has failed.
         
        Congratulation, your account has been created. An activation E-mail has been sent to you.
         
        Registration of new user accounts is disabled.
         
        Your account is now active.
         
Hello,

A request to reset your password has been received.

To reset your password, please follow this link:
 
Welcome on %(site_name)s.

An account with the user name %(username)s has been created on %(site_name)s.

To activate your account, please follow this link:
 %(site_name)s: Request to reset your password Account Settings Account activation complete Account activation failed Account activation for %(username)s on %(site_name)s Account creation Ask for a new password reset Confirm Congratulation, you succefully set your new password. Create an account If you didn't receive it, be sur you give us the email address you used to register and check your spams. Log in Log me in... Login Login again Lost your password ? New password set Password reset complete Password reset email sent Password reset request Password reset: Enter new password Password reset: Incorrect link Registration closed Registration complete Request to reset your password See you again. Send password reset link Set new password The reset of your password is complete. You can now login again. This modification link is not incorrect or have already been used. We sent you an email to reset your password. You are already connected. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 

Meilleures salutiations
––––––––
L'équipe de Swisslinux.org
 

Vous avez %(expiration_days)s jours avant l'expiration de ce lien d'activation .

Meilleures salutiations.
––––––––
L'équipe de Swisslinux.org
 
        L'activation de compte à échoué.
         
        Félicitation, votre compte à été créé. Un e-mail d'activation vous a été envoyé.
         
        L'enregistrement de nouveaux compte est désactivé.
         
        Votre compte est maintenant actif.
         
Bonjour,

Une demande pour réinitialiser votre mot de passe nous est parvenu.

Pour réinitialiser votre mote de passe, veuillez suivre ce lien:
 
Bienvenu sur %(site_name)s.

Un compte avec le nom %(username)s a été créé sur %(site_name)s.

Pour activer votre compte, veuillez suivre ce lien:
 %(site_name)s: Demande pour réinitialisation du mot de passe Paramètres du comptes Activation du compte réussie Échec de l'activation de compte Activation du compte %(username)s sûr %(site_name)s Création de compte Demander un nouveau mot de passe Confirmer Félicitation, vous avez définit votre nouveau mot de passe avec succès. Créer un compte Si vous ne l'avez pas reçu, vérifiez que vous avez saisi l'adresse avec laquelle vous vous êtes enregistré et contrôlez votre dossier de pourriels. Me connecter... Me connecter... Connexion Se connecter à nouveau Mot de passe perdu ? Nouveau mot de passe définit Réinitialisation du mot de passe complet E-mail de réinitialisation du mot de passe envoyé Demande pour réinitialisation du mot de passe Réinitialisation du mot de passe: Entrez un nouveau mot de passe Réinitialisation du mot de passe: Mauvais lien Enregistrement fermé Enregistrement complet Demande pour réinitialisation du mot de passe À la prochaine. Envoyer lien de réinitialisation du mot de passe Définir nouveau mot de passe La réinitialisation de votre mot de passe est complet. Vous pouvez vous connecter à nouveau. Ce lien de modification n'est pas correct ou a déjà été utilisé. Nous vous avons envoyé un e-mail pour réinitialiser votre mot de passe. Vous ête déjà connecté. 